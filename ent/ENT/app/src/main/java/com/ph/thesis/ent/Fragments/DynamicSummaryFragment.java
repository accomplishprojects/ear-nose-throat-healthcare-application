/*
 * Copyright (C) 2013 Andreas Stuetz <andreas.stuetz@gmail.com>
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.ph.thesis.ent.Fragments;

import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.ph.thesis.ent.Model.RecommendationModel;
import com.ph.thesis.ent.R;
import com.ph.thesis.ent.Utilities.UI.GUI.TouchImageView;

public class DynamicSummaryFragment extends Fragment {

    private static final String ARG_POSITION = "position";
    public int position;
    View mainView;
    TextView txtTitle, txtContents, txtAlternativeName;
    TouchImageView imgImageBanner;
    RelativeLayout relImageBanner;
    private OnFragmentInteractionListener listener;

    public static DynamicSummaryFragment newInstance(int position) {
        DynamicSummaryFragment f = new DynamicSummaryFragment();
        Bundle b = new Bundle();
        b.putInt(ARG_POSITION, position);
        f.setArguments(b);
        return f;
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof OnFragmentInteractionListener) {
            listener = (OnFragmentInteractionListener) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement OnFragmentInteractionListener");
        }
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        position = getArguments().getInt(ARG_POSITION);
        Log.i("RecomFragment", position + "");

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        mainView = inflater.inflate(R.layout.fragment_recommendation, container, false);
        txtAlternativeName = (TextView) mainView.findViewById(R.id.txtBannerInfo);
        txtTitle = (TextView) mainView.findViewById(R.id.txtTitle);
        txtContents = (TextView) mainView.findViewById(R.id.txtContents);
        imgImageBanner = (TouchImageView) mainView.findViewById(R.id.imgImageBanner);
        relImageBanner = (RelativeLayout) mainView.findViewById(R.id.relImageBanner);

        txtTitle.setText(RecommendationModel.getStrTitle());
        setBannerInfo();


        if (position == 0) {
            txtContents.setText("Contents");
            listener.onFragmentCreated(true, txtContents.getText().toString());
        } else if (position == 1) {
            txtTitle.setVisibility(View.GONE);
            relImageBanner.setVisibility(View.GONE);
            txtContents.setText(RecommendationModel.getStrSymptoms());
            listener.onFragmentCreated(true, txtContents.getText().toString());
        } else if (position == 2) {
            txtTitle.setVisibility(View.GONE);
            relImageBanner.setVisibility(View.GONE);
            txtContents.setText(RecommendationModel.getStrPrevention());
            listener.onFragmentCreated(true, txtContents.getText().toString());
        } else if (position == 3) {
            txtTitle.setVisibility(View.GONE);
            relImageBanner.setVisibility(View.GONE);
            txtContents.setText(RecommendationModel.getStrTreatment());
            listener.onFragmentCreated(true, txtContents.getText().toString());
        }

        return mainView;
    }

    private void setBannerInfo() {
        String strTitle = RecommendationModel.getStrTitle();

        // Arraylist of noseImages for imgImageBanner
        int[] noseImages = new int[]{
                R.drawable.banner_allergic_rhinitis,        //0
                R.drawable.banner_sinusitis,                //1
                R.drawable.banner_nasal_polyps,             //2
                R.drawable.banner_nasal_boone_fracture,     //3
                R.drawable.banner_epistaxis_2,              //4
        };

        int[] throatImages = new int[]{
                R.drawable.banner_tonsillitis,              //0
                R.drawable.banner_pharyngitis,              //1
                R.drawable.banner_thyroid_mass_malignant,   //2
                R.drawable.banner_laryngeal_mass_malignant, //3
                R.drawable.banner_thyroid_mass,             //4
        };

        int[] earImages = new int[]{
                R.drawable.banner_impacted_ccerumen,         //0
                R.drawable.banner_diffuse_otitis,            //1
                R.drawable.banner_serous_otitis,             //2
                R.drawable.banner_bells_palsy,               //3
                R.drawable.banner_chronic_suppurative,       //4
        };

        // Setting up image for imgImageBanner widget
        // NOSE
        if (strTitle.equals("Allergic Rhinitis")) {
            txtAlternativeName.setText("Alternative Name:\nHay fever - self-care; Seasonal rhinitis - self-care");
            imgImageBanner.setImageResource(noseImages[0]);
        } else if (strTitle.equals("Sinusitis")) {
            txtAlternativeName.setText("lternative Name:\nAcute sinusitis; Sinus infection; Sinusitis - acute; Sinusitis - chronic; Rhinosinusitis.");
            imgImageBanner.setImageResource(noseImages[1]);
        } else if (strTitle.equals("Nasal Polyps")) {
            txtAlternativeName.setText("Alternative Name:\nNasal polyps are soft, sac-like growths on the lining of the nose or sinuses.");
            imgImageBanner.setImageResource(noseImages[2]);
        } else if (strTitle.equals("Nasal Bone Fracture")) {
            txtAlternativeName.setText("Alternative Name: Broken nose");
            imgImageBanner.setImageResource(noseImages[3]);
        } else if (strTitle.equals("Epistaxis")) {
            txtAlternativeName.setText("Alternative Name:\nBleeding from the nose");
            imgImageBanner.setImageResource(noseImages[4]);
        }

        // THROAT
        else if (strTitle.equals("Tosilitis")) {
            imgImageBanner.setImageResource(throatImages[0]);

        } else if (strTitle.equals("Pharyngitis")) {
            txtAlternativeName.setText("Alternative Name:\nPharyngitis - bacterial; Sore throat");
            imgImageBanner.setImageResource(throatImages[1]);

        } else if (strTitle.equals("Thyroid Mass, Malignant")) {
            txtAlternativeName.setText("Alternative Name:\n" +
                    "Tumor - thyroid; Cancer – thyroid");
            imgImageBanner.setImageResource(throatImages[2]);

        } else if (strTitle.equals("Laryngeal Mass, Malignant")) {
            txtAlternativeName.setText("Alternative Name:\n" +
                    "Laryngael Cancer; Larynx Cancer; Vocal cord cancer; Cancer of the glottis; ");
            imgImageBanner.setImageResource(throatImages[3]);

        } else if (strTitle.equals("Thyroid Mass")) {
            txtAlternativeName.setText("Alternative Name:\nThyroid Nodules");
            imgImageBanner.setImageResource(throatImages[4]);
        }

        // EAR
        else if (strTitle.equals("Impacted Cerumen")) {
            imgImageBanner.setImageResource(earImages[0]);

        } else if (strTitle.equals("Diffuse Otitis Externa")) {
            txtAlternativeName.setText("Alternative Name:\nPharyngitis - bacterial; Sore throat");
            imgImageBanner.setImageResource(earImages[1]);

        } else if (strTitle.equals("Serous Otitis Media")) {
            txtAlternativeName.setText("Alternative Name:\n" +
                    "Tumor - thyroid; Cancer – thyroid");
            imgImageBanner.setImageResource(earImages[2]);

        } else if (strTitle.equals("Bell's Palsy")) {
            txtAlternativeName.setText("Alternative Name:\n" +
                    "Laryngael Cancer; Larynx Cancer; Vocal cord cancer; Cancer of the glottis; ");
            imgImageBanner.setImageResource(earImages[3]);

        } else if (strTitle.equals("Chronic Suppurative Otitis Media")) {
            txtAlternativeName.setText("Alternative Name:\nThyroid Nodules");
            imgImageBanner.setImageResource(earImages[4]);
        }

    }


    public interface OnFragmentInteractionListener {
        public void onFragmentCreated(boolean status, String content);
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
    }
}