package com.ph.thesis.ent.Activity;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;

import com.ph.thesis.ent.R;

/**
 * Created by vidalbenjoe on 30/01/2016.
 */
public class SplashScreenActivity extends AppCompatActivity {
    private static String TAG = SplashScreenActivity.class.getName();
    private static long SLEEP_TIME = 2;    // Sleep for some time

    @Override
    protected void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        setContentView(R.layout.splash_screen_layout);


        // Start timer and launch main activity
        IntentLauncher launcher = new IntentLauncher();
        launcher.start();
    }


    private class IntentLauncher extends Thread {
        @Override
        /**
         * Sleep for some time and than start new activity.
         */
        public void run() {
            try {
                // Sleeping
                Thread.sleep(SLEEP_TIME * 1200);
            } catch (Exception e) {
                Log.e(TAG, e.getMessage());
            }
            Intent mainActivityIntent = new Intent(getApplicationContext(), MainActivity.class);
            startActivity(mainActivityIntent);
            SplashScreenActivity.this.finish();

        }
    }
}
