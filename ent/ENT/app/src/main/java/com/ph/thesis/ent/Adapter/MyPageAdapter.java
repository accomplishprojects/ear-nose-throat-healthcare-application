package com.ph.thesis.ent.Adapter;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;

import com.ph.thesis.ent.Fragments.DynamicSummaryFragment;

/**
 * Created by luigigo on 2/3/16.
 */
public class MyPageAdapter extends FragmentStatePagerAdapter{

    private final String[] TITLES = {"Introduction", "Symptoms", "Prevention", "Treatment"};

    public MyPageAdapter(FragmentManager fm) {
        super(fm);
    }

    @Override
    public CharSequence getPageTitle(int position) {
        return TITLES[position];
    }

    @Override
    public int getCount() {
        return TITLES.length;
    }

    @Override
    public Fragment getItem(int position) {
        return DynamicSummaryFragment.newInstance(position);
    }

}
