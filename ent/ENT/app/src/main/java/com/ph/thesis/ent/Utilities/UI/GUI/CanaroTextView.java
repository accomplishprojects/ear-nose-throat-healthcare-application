package com.ph.thesis.ent.Utilities.UI.GUI;

import android.content.Context;
import android.util.AttributeSet;
import android.widget.TextView;

import com.ph.thesis.ent.Utilities.UI.AppController;

/**
 * Created by vidalbenjoe on 30/01/2016.
 */
public class CanaroTextView extends TextView {
    public CanaroTextView(Context context) {
        this(context, null);
    }

    public CanaroTextView(Context context, AttributeSet attrs) {
        this(context, attrs, 0);
    }

    public CanaroTextView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        setTypeface(AppController.canaroExtraBold);
    }

}
