package com.ph.thesis.ent.Fragments;

import android.content.Intent;
import android.database.Cursor;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.ph.thesis.ent.Database.DatabaseAccess;
import com.ph.thesis.ent.Model.RecommendationModel;
import com.ph.thesis.ent.R;
import com.ph.thesis.ent.Utilities.UI.Tools.CustomUtils;

import java.util.Arrays;
import java.util.List;

/**
 * Created by luigigo on 1/31/16.
 */
public class QuestionFragment extends Fragment implements View.OnClickListener {

    DatabaseAccess databaseAccess;
    TextView txtQuestion;
    Button btnYes, btnNo;
    View mainView;
    Cursor cursor;
    public static String questionIndex = "1";
    List arrayQuestionValue;
    Intent i;
    String TAG = "QuestionFragment";
    public static String strTableIndicator = null;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        mainView = inflater.inflate(R.layout.fragment_question, container, false);
        databaseAccess = DatabaseAccess.getInstance(getActivity());
        initVars();
        return mainView;
    }

    private void initVars() {
        txtQuestion = (TextView) mainView.findViewById(R.id.txtQuestion);
        btnYes = (Button) mainView.findViewById(R.id.btnYes);
        btnNo = (Button) mainView.findViewById(R.id.btnNo);
        btnYes.setOnClickListener(this);
        btnNo.setOnClickListener(this);

        databaseAccess.open();
        Cursor cursor = databaseAccess.getInitialQuestion(questionIndex, strTableIndicator);

        if (cursor != null) {
            try {
                String strQuestion = cursor.getString(cursor.getColumnIndex(cursor.getColumnName(0)));
                txtQuestion.setText(strQuestion);
                databaseAccess.close();
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.btnYes:
                if (strTableIndicator.equals("tbl_nose"))
                    arrayQuestionValue = Arrays.asList("13", "23", "15", "16", "17");
                else if (strTableIndicator.equals("tbl_throat"))
                    arrayQuestionValue = Arrays.asList("5", "11", "15", "18", "10");
                else if (strTableIndicator.equals("tbl_ear"))
                    arrayQuestionValue = Arrays.asList("", "16", "12", "13", "15", "23");
                if (arrayQuestionValue.contains(questionIndex)) {
                    databaseAccess.open();

                    if (strTableIndicator.equals("tbl_nose")) {
                        if (questionIndex.equals("23")) {
                            int contentIndex = 1 + Integer.parseInt(questionIndex);
                            cursor = databaseAccess.getContent(String.valueOf(contentIndex), strTableIndicator);
                            Log.i(TAG + " ContentIndex", contentIndex + "");
                        } else {
                            int contentIndex = 5 + Integer.parseInt(questionIndex);
                            cursor = databaseAccess.getContent(String.valueOf(contentIndex), strTableIndicator);
                            Log.i(TAG + " ContentIndex", contentIndex + "");
                        }
                    } else if (strTableIndicator.equals("tbl_throat")) {
                        if (questionIndex.equals("15") || questionIndex.equals("18")) {
                            int contentIndex = 1 + Integer.parseInt(questionIndex);
                            cursor = databaseAccess.getContent(String.valueOf(contentIndex), strTableIndicator);
                            Log.i(TAG + " ContentIndex", contentIndex + "");
                        } else {
                            int contentIndex = 3 + Integer.parseInt(questionIndex);
                            cursor = databaseAccess.getContent(String.valueOf(contentIndex), strTableIndicator);
                            Log.i(TAG + " ContentIndex", contentIndex + "");
                        }
                    } else if (strTableIndicator.equals("tbl_ear")) {
                        if (questionIndex.equals("23")) {
                            int contentIndex = 1 + Integer.parseInt(questionIndex);
                            cursor = databaseAccess.getContent(String.valueOf(contentIndex), strTableIndicator);
                            Log.i(TAG + " ContentIndex", contentIndex + "");
                        } else {
                            int contentIndex = 5 + Integer.parseInt(questionIndex);
                            cursor = databaseAccess.getContent(String.valueOf(contentIndex), strTableIndicator);
                            Log.i(TAG + " ContentIndex", contentIndex + "");
                        }
                    }

                    if (cursor != null) {
                        String strSymptoms, strPrevention, strTreatment, strTitle, strContentID;
                        strContentID = cursor.getString(cursor.getColumnIndex(cursor.getColumnName(0)));
                        strTitle = cursor.getString(cursor.getColumnIndex(cursor.getColumnName(1)));
                        strSymptoms = cursor.getString(cursor.getColumnIndex(cursor.getColumnName(8)));
                        strPrevention = cursor.getString(cursor.getColumnIndex(cursor.getColumnName(9)));
                        strTreatment = cursor.getString(cursor.getColumnIndex(cursor.getColumnName(10)));

                        Log.i(TAG + " Contents", strSymptoms + " " + strPrevention + " " + strTreatment);

                        RecommendationModel.setStrContentID(strContentID);
                        RecommendationModel.setStrTitle(strTitle);
                        RecommendationModel.setStrSymptoms(strSymptoms);
                        RecommendationModel.setStrPrevention(strPrevention);
                        RecommendationModel.setStrTreatment(strTreatment);

                        CustomUtils.loadFragment(new SummaryFragment(), true, getActivity());
                        questionIndex = "1";
                    }

                } else {
                    databaseAccess.open();
                    cursor = databaseAccess.getQuestionIfYes(questionIndex, strTableIndicator, getActivity());

                    Log.i(TAG + " Cursor", cursor.getString(Integer.parseInt(cursor.getColumnIndex(cursor.getColumnName(0)) + "")));
                    if (cursor != null) {
                        try {
                            String strQuestion = cursor.getString(cursor.getColumnIndex(cursor.getColumnName(0)));
                            txtQuestion.setText(strQuestion);
                            databaseAccess.close();
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    }
                }
                break;

            case R.id.btnNo:
                if (strTableIndicator.equals("tbl_nose"))
                    arrayQuestionValue = Arrays.asList("13", "23", "15", "16", "17");
                else if (strTableIndicator.equals("tbl_throat"))
                    arrayQuestionValue = Arrays.asList("5", "11", "18", "10");
                else if (strTableIndicator.equals("tbl_ear"))
                    arrayQuestionValue = Arrays.asList("", "16", "12", "13", "15", "23");

                if (arrayQuestionValue.contains(questionIndex)) {
                    Toast.makeText(getActivity(), "Invalid", Toast.LENGTH_SHORT).show();
                } else {
                    databaseAccess.open();
                    cursor = databaseAccess.getQuestionIfNo(questionIndex, strTableIndicator);

                    Log.i("Cursor", cursor.getString(Integer.parseInt(cursor.getColumnIndex(cursor.getColumnName(0)) + "")));
                    if (cursor != null) {
                        try {
                            String strQuestion = cursor.getString(cursor.getColumnIndex(cursor.getColumnName(0)));
                            txtQuestion.setText(strQuestion);
                            databaseAccess.close();
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    }
                }
                break;
        }
    }

}
