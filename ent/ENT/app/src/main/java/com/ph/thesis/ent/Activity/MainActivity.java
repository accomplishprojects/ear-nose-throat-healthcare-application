package com.ph.thesis.ent.Activity;

import android.content.Intent;
import android.os.Bundle;
import android.speech.tts.TextToSpeech;
import android.support.v4.app.FragmentManager;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.Toast;

import com.michaldrabik.tapbarmenulib.TapBarMenu;
import com.ph.thesis.ent.Database.DatabaseAccess;
import com.ph.thesis.ent.Fragments.DynamicSummaryFragment;
import com.ph.thesis.ent.Fragments.QuestionFragment;
import com.ph.thesis.ent.Fragments.SummaryFragment;
import com.ph.thesis.ent.Model.RecommendationModel;
import com.ph.thesis.ent.R;
import com.ph.thesis.ent.Utilities.UI.GUI.CanaroTextView;
import com.ph.thesis.ent.Utilities.UI.Tools.Constants;
import com.ph.thesis.ent.Utilities.UI.Tools.CustomUtils;
import com.yalantis.guillotine.animation.GuillotineAnimation;

import java.util.Locale;

public class MainActivity extends AppCompatActivity implements View.OnClickListener, DynamicSummaryFragment.OnFragmentInteractionListener, SummaryFragment.OnFragmentInteractionListener, TextToSpeech.OnInitListener {

    View contentHamburger;
    View guillotineMenu;
    Toolbar toolbar;
    FrameLayout root;
    LinearLayout linearEar, linearNose, linearThroat;
    private static final long RIPPLE_DURATION = 250;
    CanaroTextView txtDisplay;
    DatabaseAccess databaseAccess;
    TapBarMenu tapBarMenu;
    ImageView imgAudio, imgVideo;
    public static String _content;
    private TextToSpeech myTTS;
    public int pagerPosition;
    public boolean isAudioPlayed = false;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        databaseAccess = DatabaseAccess.getInstance(this);
        CustomUtils.checkTTSdata(this);
        initVars();
        setMenu();

    }

    private void initVars() {
        toolbar = (Toolbar) findViewById(R.id.toolbar);
        root = (FrameLayout) findViewById(R.id.root);
        contentHamburger = (View) findViewById(R.id.content_hamburger);
        txtDisplay = (CanaroTextView) findViewById(R.id.textDisplay);

        if (toolbar != null) {
            setSupportActionBar(toolbar);
            getSupportActionBar().setTitle(null);
        }

        guillotineMenu = LayoutInflater.from(this).inflate(R.layout.guillotine_layout, null);
        root.addView(guillotineMenu);

        linearEar = (LinearLayout) findViewById(R.id.linearEar);
        linearNose = (LinearLayout) findViewById(R.id.linearNose);
        linearThroat = (LinearLayout) findViewById(R.id.linearThroat);
        tapBarMenu = (TapBarMenu) findViewById(R.id.tapBarMenu);
        imgAudio = (ImageView) findViewById(R.id.imgAudio);
        imgVideo = (ImageView) findViewById(R.id.imgVideo);

        tapBarMenu.setVisibility(View.GONE);
        linearEar.setOnClickListener(this);
        linearNose.setOnClickListener(this);
        linearThroat.setOnClickListener(this);
        tapBarMenu.setOnClickListener(this);
        imgAudio.setOnClickListener(this);
    }

    private void setMenu() {
        new GuillotineAnimation.GuillotineBuilder(guillotineMenu, guillotineMenu.findViewById(R.id.guillotine_hamburger), contentHamburger)
                .setActionBarViewForAnimation(toolbar)
                .setStartDelay(RIPPLE_DURATION)
                .setClosedOnStart(true)
                .build();
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {

            case R.id.linearEar:
                tapBarMenu.setVisibility(View.GONE);
                QuestionFragment.questionIndex = "1";
                QuestionFragment.strTableIndicator = "tbl_ear";
                CustomUtils.loadFragment(new QuestionFragment(), true, this);
                setMenu();
                break;

            case R.id.linearNose:
                tapBarMenu.setVisibility(View.GONE);
                QuestionFragment.questionIndex = "1";
                QuestionFragment.strTableIndicator = "tbl_nose";
                CustomUtils.loadFragment(new QuestionFragment(), true, this);
                setMenu();
                break;

            case R.id.linearThroat:
                tapBarMenu.setVisibility(View.GONE);
                QuestionFragment.questionIndex = "1";
                QuestionFragment.strTableIndicator = "tbl_throat";
                CustomUtils.loadFragment(new QuestionFragment(), true, this);
                setMenu();
                break;

            case R.id.imgAudio:
                if (isAudioPlayed) {
                    myTTS.stop();
                    imgAudio.setImageDrawable(getResources().getDrawable(R.mipmap.ic_audio_mute));
                    isAudioPlayed = false;
                } else {
                    switch (pagerPosition) {
                        case 0:
                            MainActivity._content = "Contents";
                            break;

                        case 1:
                            MainActivity._content = RecommendationModel.getStrSymptoms();
                            break;

                        case 2:
                            MainActivity._content = RecommendationModel.getStrPrevention();
                            break;

                        case 3:
                            MainActivity._content = RecommendationModel.getStrTreatment();
                            break;
                    }

                    myTTS.speak(_content, TextToSpeech.QUEUE_FLUSH, null);
                    imgAudio.setImageDrawable(getResources().getDrawable(R.mipmap.ic_audio));
                    isAudioPlayed = true;
                }
                break;

            case R.id.tapBarMenu:
                tapBarMenu.toggle();
                break;

        }

    }

    @Override
    public void onBackPressed() {
        FragmentManager fm = getSupportFragmentManager();
        for (int i = 0; i < fm.getBackStackEntryCount(); ++i) {
            fm.popBackStack();
            tapBarMenu.setVisibility(View.GONE);
            myTTS.stop();

        }

    }

    @Override
    public void onFragmentCreated(boolean status, String content) {

        if (status) {
            tapBarMenu.setVisibility(View.VISIBLE);
        } else {
            tapBarMenu.setVisibility(View.GONE);
        }

    }


    @Override
    public void onInit(int i) {

        //check for successful instantiation
        if (i == TextToSpeech.SUCCESS) {
            if (myTTS.isLanguageAvailable(Locale.UK) == TextToSpeech.LANG_AVAILABLE)
                myTTS.setLanguage(Locale.UK);
        } else if (i == TextToSpeech.ERROR) {
            Toast.makeText(this, "Sorry! Text To Speech failed...", Toast.LENGTH_LONG).show();
        }
    }

    public void onActivityResult(int requestCode, int resultCode, Intent data) {

        if (requestCode == Constants.MY_DATA_CHECK_CODE) {
            if (resultCode == TextToSpeech.Engine.CHECK_VOICE_DATA_PASS) {
                //the user has the necessary data - create the TTS
                myTTS = new TextToSpeech(this, this);
            } else {
                //no data - install it now
                Intent installTTSIntent = new Intent();
                installTTSIntent.setAction(TextToSpeech.Engine.ACTION_INSTALL_TTS_DATA);
                startActivity(installTTSIntent);
            }
        }
    }

    @Override
    public void onFragmentCreatedSummary(boolean status, int position) {
        isAudioPlayed = status;
        pagerPosition = position;

        if (isAudioPlayed) {
            myTTS.stop();
            imgAudio.setImageDrawable(getResources().getDrawable(R.mipmap.ic_audio_mute));
            isAudioPlayed = false;
        } else {
            switch (position) {
                case 0:
                    MainActivity._content = "Contents";
                    break;

                case 1:
                    MainActivity._content = RecommendationModel.getStrSymptoms();
                    break;

                case 2:
                    MainActivity._content = RecommendationModel.getStrPrevention();
                    break;

                case 3:
                    MainActivity._content = RecommendationModel.getStrTreatment();
                    break;
            }

            myTTS.speak(_content, TextToSpeech.QUEUE_FLUSH, null);
            imgAudio.setImageDrawable(getResources().getDrawable(R.mipmap.ic_audio));
            isAudioPlayed = true;
        }
    }
}
