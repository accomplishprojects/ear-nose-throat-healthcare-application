package com.ph.thesis.ent.Utilities.UI;

import android.app.Application;
import android.graphics.Typeface;

/**
 * Created by vidalbenjoe on 30/01/2016.
 */
public class AppController extends Application {
    private static final String CANARO_EXTRA_BOLD_PATH = "fonts/canaro_extra_bold.otf";
    public static Typeface canaroExtraBold;

    @Override
    public void onCreate() {
        super.onCreate();
        initTypeface();
    }

    private void initTypeface() {
        canaroExtraBold = Typeface.createFromAsset(getAssets(), CANARO_EXTRA_BOLD_PATH);

    }
}