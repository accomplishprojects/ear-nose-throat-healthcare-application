package com.ph.thesis.ent.Utilities.UI.Tools;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.speech.tts.TextToSpeech;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentTransaction;

import com.ph.thesis.ent.R;

/**
 * Created by luigigo on 1/31/16.
 */
public class CustomUtils {

    public static void loadFragment(final android.support.v4.app.Fragment fragment, boolean addToBackStack, Context context) {
        final FragmentTransaction fragmentTransaction = ((FragmentActivity) context).getSupportFragmentManager()
                .beginTransaction()
                .replace(R.id.frame_content, fragment, fragment.getClass().getName());
        if (addToBackStack) {
            fragmentTransaction.addToBackStack(fragment.getClass().getName());
        }
//        CustomUtils.popBackStack(fragment);
        fragmentTransaction.commit();
    }

    public static void checkTTSdata(Activity activity) {
        //check for TTS data
        Intent checkTTSIntent = new Intent();
        checkTTSIntent.setAction(TextToSpeech.Engine.ACTION_CHECK_TTS_DATA);
        activity.startActivityForResult(checkTTSIntent, Constants.MY_DATA_CHECK_CODE);
    }

}
