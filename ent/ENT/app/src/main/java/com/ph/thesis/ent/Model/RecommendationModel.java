package com.ph.thesis.ent.Model;

/**
 * Created by luigigo on 2/3/16.
 */
public class RecommendationModel {

    public static String strSymptoms;

    public static String getStrSymptoms() {
        return strSymptoms;
    }

    public static void setStrSymptoms(String strSymptoms) {
        RecommendationModel.strSymptoms = strSymptoms;
    }

    public static String getStrPrevention() {
        return strPrevention;
    }

    public static void setStrPrevention(String strPrevention) {
        RecommendationModel.strPrevention = strPrevention;
    }

    public static String getStrTreatment() {
        return strTreatment;
    }

    public static void setStrTreatment(String strTreatment) {
        RecommendationModel.strTreatment = strTreatment;
    }

    public static String strPrevention;
    public static String strTreatment;
    public static String strTitle;
    public static String strContentID;

    public static String getStrContentID() {
        return strContentID;
    }

    public static void setStrContentID(String strContentID) {
        RecommendationModel.strContentID = strContentID;
    }

    public static String getStrTitle() {
        return strTitle;
    }

    public static void setStrTitle(String strTitle) {
        RecommendationModel.strTitle = strTitle;
    }
}
