package com.ph.thesis.ent.Database;

import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

import com.ph.thesis.ent.Fragments.QuestionFragment;

/**
 * Created by luigigo on 2/1/16.
 */
public class DatabaseAccess {
    private SQLiteOpenHelper openHelper;
    private SQLiteDatabase database;
    private static DatabaseAccess instance;

    /**
     * Private constructor to aboid object creation from outside classes.
     *
     * @param context
     */
    private DatabaseAccess(Context context) {
        this.openHelper = new DatabaseOpenHelper(context);
    }

    /**
     * Return a singleton instance of DatabaseAccess.
     *
     * @param context the Context
     * @return the instance of DabaseAccess
     */
    public static DatabaseAccess getInstance(Context context) {
        if (instance == null) {
            instance = new DatabaseAccess(context);
        }
        return instance;
    }

    /**
     * Open the database connection.
     */
    public void open() {
        this.database = openHelper.getWritableDatabase();
    }

    /**
     * Close the database connection.
     */
    public void close() {
        if (database != null) {
            this.database.close();
        }
    }

    /**
     * Read all quotes from the database.
     *
     * @return a List of questions
     */
    public Cursor getQuestionIfYes(String id, String strTableIndicator, Context context) {
        String selectQuestion, strYesCol, strId;
        Cursor cursor;
        strYesCol = "SELECT _id, if_yes FROM " + strTableIndicator + " WHERE _id=?";
        cursor = database.rawQuery(strYesCol, new String[]{id});

        if (cursor != null) {
            cursor.moveToFirst();
            strId = cursor.getString(cursor.getColumnIndex(cursor.getColumnName(0)));
            strYesCol = cursor.getString(cursor.getColumnIndex(cursor.getColumnName(1)));

            QuestionFragment.questionIndex = strYesCol;
            Log.i("CursorDB", strId + " " + strYesCol);
            selectQuestion = "SELECT question FROM " + strTableIndicator + " WHERE _id=?";
            cursor = database.rawQuery(selectQuestion, new String[]{strYesCol});

            if (cursor != null) {
                cursor.moveToFirst();
                Log.i("CursorDB", cursor.getString(cursor.getColumnIndex(cursor.getColumnName(0))) + "");
            }

        } else {
            Log.i("itemtotalCur:", String.valueOf(cursor));
        }
        return cursor;

    }

    public Cursor getQuestionIfNo(String id, String strTableIndicator) {
        String selectQuestion, strNoCol, strId;
        Cursor cursor;
        strNoCol = "SELECT _id, if_no FROM " + strTableIndicator + " WHERE _id=?";
        cursor = database.rawQuery(strNoCol, new String[]{id});

        if (cursor != null) {
            cursor.moveToFirst();
            strId = cursor.getString(cursor.getColumnIndex(cursor.getColumnName(0)));
            strNoCol = cursor.getString(cursor.getColumnIndex(cursor.getColumnName(1)));
            QuestionFragment.questionIndex = strNoCol;
            Log.i("CursorDB", strId + " " + strNoCol);

            if (strNoCol != null) {
                selectQuestion = "SELECT question FROM " + strTableIndicator + " WHERE _id=?";
                cursor = database.rawQuery(selectQuestion, new String[]{strNoCol});

                if (cursor != null) {
                    cursor.moveToFirst();
                    Log.i("CursorDB", cursor.getString(cursor.getColumnIndex(cursor.getColumnName(0))) + "");
                }
            }
        } else {
            Log.i("itemtotalCur:", String.valueOf(cursor));
        }
        return cursor;

    }

    public Cursor getInitialQuestion(String id, String strTableIndicator) {
        String selectQuestion = "SELECT question FROM " + strTableIndicator + " WHERE _id=?";
        Cursor cursor = database.rawQuery(selectQuestion, new String[]{id});

        if (cursor != null) {
            cursor.moveToFirst();
        } else {
            Log.i("itemtotalCur:", String.valueOf(cursor));
        }

        return cursor;
    }

    public Cursor getContent(String id, String strTableIndicator) {
        String content = "SELECT * FROM " + strTableIndicator + " WHERE _id=?";
        Cursor cursor = database.rawQuery(content, new String[]{id});

        if (cursor != null) {
            cursor.moveToFirst();
        }

        return cursor;
    }
}
