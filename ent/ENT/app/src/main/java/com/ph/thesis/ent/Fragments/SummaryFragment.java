package com.ph.thesis.ent.Fragments;

import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.view.ViewPager;
import android.util.TypedValue;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.astuetz.PagerSlidingTabStrip;
import com.ph.thesis.ent.Adapter.MyPageAdapter;
import com.ph.thesis.ent.R;

/**
 * Created by luigigo on 2/4/16.
 */
public class SummaryFragment extends Fragment {

    PagerSlidingTabStrip tabs;
    ViewPager pager;
    MyPageAdapter adapter;
    boolean isScrollHorizontally = false;
    int _position = 0;
    OnFragmentInteractionListener listener;

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof OnFragmentInteractionListener) {
            listener = (OnFragmentInteractionListener) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement OnFragmentInteractionListener");
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View mainView = inflater.inflate(R.layout.activity_recommendation, container, false);
        tabs = (PagerSlidingTabStrip) mainView.findViewById(R.id.tabs);
        pager = (ViewPager) mainView.findViewById(R.id.pager);
        adapter = new MyPageAdapter(getActivity().getSupportFragmentManager());
        pager.setAdapter(adapter);

        final int pageMargin = (int) TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, 4, getResources()
                .getDisplayMetrics());
        pager.setPageMargin(pageMargin);
        tabs.setViewPager(pager);
        listener.onFragmentCreatedSummary(true, 0);
        pager.setOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

            }

            @Override
            public void onPageSelected(int position) {
                listener.onFragmentCreatedSummary(true, position);
            }

            @Override
            public void onPageScrollStateChanged(int state) {

            }
        });
        return mainView;
    }

    public interface OnFragmentInteractionListener {
        public void onFragmentCreatedSummary(boolean status, int position);
    }
}
